<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            $Log = new \App\Http\Models\gerbera\Log;
            $Gn = new \App\Http\Controllers\gerbera\generalController;
            $logs = $Log->where('status', '!=', 'logged out')->with(['log_actions'])->get();

            foreach ($logs as $v) {
                $updated_log = $Gn->update_logs($v->log_actions['content']);
                $v->update($updated_log);
            }

            DB::connection('mytestdb')->table('testgerbera')->insert([
                'title' => 'test cron for updating logs',
                'content' => 'working',
                'created_at' => date('Y-m-d H:i:s')
            ]);

        })->cron('*/1 * * * *'); 
    }
}
