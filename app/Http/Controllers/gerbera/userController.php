<?php namespace App\Http\Controllers\gerbera;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

class userController extends Controller
{

	public function index(){
		$usersObj = new \App\Http\Models\gerbera\User;
		$gn = new \App\Http\Controllers\gerbera\generalController;
		$users = $usersObj->with(['settings'])->get();
		//$gn->pre($users->toArray());
		return view('gerbera.user.index',compact('users','gn'));
	}

	public function destroy($id){
		$usersObj = new \App\Http\Models\gerbera\User;
		$user = $usersObj->where('id',$id)->with('settings')->first();
		if($user->settings)$user->settings->delete();
		$user->delete();
		return redirect()->back();
	}

	public function update(Request $r,$id){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$usersObj = new \App\Http\Models\gerbera\User;
		$user =	$usersObj->find($id);
		$input = $r->all();
		//$x = $gn->userOptions_putter($user->options,'campaign_id',$r->input('campaign_id')); $gn->pre($x);
		$input['options'] = $gn->userOptions_putter($user->options,'campaign_id',$r->input('campaign_id'));
		$user->update($input);
		return redirect()->back();

	}

	public function create(Request $r){

		$gn = new \App\Http\Controllers\gerbera\generalController;
		$usersObj = new \App\Http\Models\gerbera\User;
		$input = $r->all();
		$input['password'] = bcrypt($r->input('password'));
		$options['campaign_id'] = $r->input('campaign_id');
		$input['options'] = json_encode($options);
		$user = $usersObj->create($input);
		$user->settings()->create(['user_type' => $r->input('user_type'),'password' => $r->input('password')]);
		return redirect()->back();

	}

}