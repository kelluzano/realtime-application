<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class plumAuthOps
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 

        if(!in_array(Auth::user()->user_type,['admin','operation'])):
            return redirect()->back();
        endif;
        return $next($request); 

    }
}
