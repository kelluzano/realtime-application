<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'Board list')

@section('header-scripts')
 <link rel="stylesheet" type="text/css" href="{{$asset}}timepicker2/jquery.datetimepicker.css"/>
@endsection

@section('content')
 <div class="right_col" role="main">
  <div class="row">

    <div class="col-md-7">
      <div class="x_panel ">
        <div class="x_title">
          <h2>Result</h2>
          <div class="clearfix"></div>
        </div>
      
        <div class="x_content">
        </div>
      </div>

    </div>

    @if($report_type == 'daily')

      <div class="col-md-5">
        {!!view('gerbera.reports.generate-form',compact('campaigns'))!!}
      </div>

    @endif

    @if($report_type == 'agentlog')

      <div class="col-md-5">
        {!!view('gerbera.reports.agentlog-form',compact('campaigns','users'))!!}
      </div>

    @endif
 

  </div>
</div>
@endsection 

@section('footer-scripts')
  <script src="{{$asset}}timepicker2/build/jquery.datetimepicker.full.js"></script>
  <script type="text/javascript">
      var date = $('.datetimepicker').datetimepicker({
        timeFormat: 'Y-m-d HH:mm:ss'
      });
  </script>
@endsection