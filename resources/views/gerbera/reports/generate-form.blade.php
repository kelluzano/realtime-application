  <div class="x_panel ">
    <div class="x_title">
      <h2>Generate Reports</h2>
      <div class="clearfix"></div>
    </div>
    
    <div class="x_content">
      <br />
      <form class="form-horizontal" role="form" method="POST" action="{{route('gerbera.reports.generate_post')}}">
        {{ csrf_field() }}

        <div class="form-group">
          <label class="control-label col-md-2" for="first-name">From<span class="required">*</span>
          </label>
          <div class="col-md-10">
            <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-2" for="last-name">To<span class="required">*</span>
          </label>
          <div class="col-md-10">
            <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-2">Type:</label>
          <div class="col-md-10">
            <select class="form-control" name="reporttype">
              <option value="agent-time">Agent Time Records</option>
            </select>
          </div>
        </div>
        <div class="ln_solid"></div>

        <div class="col-md-4">
            <div class="form-group">
                <button type="button" style = "width:100%" class="btn btn-primary " data-toggle="modal" data-target="#myModal"><i class="fa fa-file-text" aria-hidden="true"></i>  Generate
                </button></h1>
            </div>
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h3 class="modal-title" id="myModalLabel">Campaign List</h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                      <div class="form-group row">
                        <div class="col-sm-12"></div> 
                          <div>
                            @foreach($campaigns as $c)
                              <?php $campaign = json_decode($c); ?>
                                <div class="col-md-3" style = "text-align:left;">
                                    <div class="radio" >
                                        <label style ="color:black">
                                            <input  type="checkbox" name="campaign[]" value="{{$campaign->id}}" >{{$campaign->name}}
                                        </label>
                                    </div>                                               
                                </div>
                              @endforeach   
                          </div>
                      </div>  

                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" style="width:100px;">Filter now</button>
                      </div>
                    
                </div>
              </div>
            </div>
          </div>

      </form>

    </div>
  </div>