<?php $asset = URL::asset('/'); ?>  
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Gentallela Alela! | </title>

  <link href="{{$asset}}gentella/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{$asset}}gentella/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="{{$asset}}gentella/css/animate.min.css" rel="stylesheet">
  <link href="{{$asset}}gentella/css/custom.css" rel="stylesheet">
  <link href="{{$asset}}gentella/css/icheck/flat/green.css" rel="stylesheet">
  <script src="{{$asset}}gentella/js/jquery.min.js"></script>
  @yield('header-scripts')
</head>
    <style type="text/css">

        .nav.side-menu li{
            cursor: pointer; 
        }
        .nav_menu{
            display: none;
        }

        body.nav-sm .container.body .left_col {

            background-color: #9B59B6;
        }

        .site_title {

            background-color: #9B59B6;
            height: 100%;
        }

        body.nav-sm ul.nav.child_menu {

            background: #9B59B6;
        }

        .nav.side-menu > li.active > a {

            background: #9B59B6;

        }

        .nav.side-menu > li.active {
            border-right: #23527c;
        }

        body.nav-sm .navbar.nav_title a i {

            filter: hue-rotate(90deg);

        }

        body.nav-sm .navbar.nav_title a i:hover {

            filter: none;

        }

        .wrapper{

          margin: 10%;
          margin-top: 10%;
        }

        html{

          background: url(<?php echo $asset ?>gentella/images/work-happy.jpg)  no-repeat center center fixed; 
          -webkit-background-size: cover;
          -moz-background-size: cover;
          -o-background-size: cover;
          background-size: cover;
          color:  rgba(0,0,0,.2)
        
        }

        .divbg{

          background-color: rgba(0,0,0,.47);
          color: white;
        }

        body {
          background: #f4eedf;
          padding: 0;
          text-align: center;
          font-family: 'open sans';
          position: relative;
          margin: 0;
          height: 100%;
        }
    
        .main h1 {
         
          font-size: 45px;
          box-sizing: border-box;
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          font-weight: 100;
          margin: 0;
        }

        .btn-rta-submit{

          width: 50%; 
          background-color:transparent;
          border: solid;
          padding: 10px;
        }

        .btn-rta-submit:hover{

         
          background-color: #3498db ;
          border: solid #3498db ;
          color:white;
        
        }

        .btn-filter{

          background-color: transparent;

        }

        #timer, #timer2{

          color:#60AEFF;
        }

        .ind{

          color:#ffb160;
        }


        .time{

          color:#8bd6f2;
        }

        .time_label{

          color: #f2a78b;
        }


    </style>


    <script> 

      setInterval(function() {
        var d = new Date();

          diem1 = "AM";
          var h1 = d.getHours();

          if (h1 == 0) {
              h1 = 12
          } else if (h1 > 12) {
              h1 = h1 - 12;
              diem1 = "PM";
          }

          $('#timer').text((h1 +':' + d.getMinutes() + ':' + d.getSeconds() + ' ' +  diem1 ));

          var currentTime = new Date();
          currentTime.setHours(currentTime.getHours()+(-3));
          var diem = "AM";
          var h = currentTime.getHours();
          var m = currentTime.getMinutes();
          var s = currentTime.getSeconds();

          if (h == 0) {
              h = 12
          } else if (h > 12) {
              h = h - 12;
              diem = "PM";
          }

          if (h < 10) {
              h = "0" + h;
          }

          if (m < 10) {
              m = "0" + m;
          }

          if (s < 10) {
              s = "0" + s;
          }

          pst = h + ":" + m + ":" + s + " " + diem
          $('#timer2').text(pst);
  
      }, 1000);
      
      
    </script> 

      <body style="background:transparent; color:white;"  class="nav-sm">

        <div class="">
          <a class="hiddenanchor" id="toregister"></a>
          <a class="hiddenanchor" id="tologin"></a> 
          <div class ="row" style ="text-align:left; margin:10px; font-size: 20px;">
            <div class="col-xs-12 " >
              
              <span class = "pull-right">
                <img class ="" src = "{{$asset}}gentella/images/website_logo0.png" style ="width:50px;"> Magellan Solutions
              </span>
           <!--    <label class ="ind pull-left">MNL :  </label>
              <label id="timer" class ="pull-left"></label>
              <label class ="ind pull-left"> | PST :  </label>
              <label id="timer2" class ="pull-left"></label> -->
            </div>
          </div>
          <div class="wrapper" >
            @yield('content')
          </div>
        </div>
      </body>

    <script type="text/javascript">

      $(document).ready(function() {
          $('.child_menu').css("display","none");
          $('.nav.side-menu > li').removeClass("active");
    });

</script>
  @yield('footer-scripts')
</html>
