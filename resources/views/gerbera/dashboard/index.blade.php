<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')

<?php 
$status = ['meeting' => 'meeting','lunch' => 'lunch','break' => 'break','other' => 'other'];
?>
    @if($authdata)
      <div class="right_col" role="main">

        <div class="row">
          @if($data)
          <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.data.update',$data->id)}}" novalidate>
          {{ csrf_field() }}          
              <div class="col-md-10">
                <div class="dashboard_graph">
                  <div class="row x_title">
                  </div>

                  <div class="col-md-12"> 
                    @foreach(json_decode($data->content,true) as $k => $v)
                      @if($k)
                      <div class="col-md-6 form-horizontal">
                        <div class="form-group">
                          <label class="control-label col-md-4" >{{str_replace(["_"], " ", $k)}} <span class="required">*</span>
                          </label>
                          <div class="col-md-8">
                            <input type="text"  class="form-control col-md-7 col-xs-12" name="{{$k}}" value="{{$v}}">
                          </div>
                        </div>

                      </div>
                      @endif
                    @endforeach
                  </div>

                  <div class="clearfix"></div>
                  <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target=".modalSave">Save</button>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-md-2">

                <div class="mail_list">
                  <div class="right mb-tc-div">
                    <h3>Timeclock</h3>
                    <p><span>break</span>{{$session['break']}}</p>
                    <p><span>lunch</span>{{$session['lunch']}}</p>
                    <p><span>meeting</span>{{$session['meeting']}}</p>
                    <p><span>other</span>{{$session['other']}}</p>
                  </div>
                </div>
                <div class="mail_list">
                  <div class="right">
                    <h3>Time started</h3>
                    <p>{{$session['login']}}</p>
                  </div>
                </div>
                <div class="mail_list">
                  <div class="right">
                    <h3>Done</h3>
                    <p>{{$metadatas['done']}} ({{$metadatas['donep']}}%)</p>
                  </div>
                </div> 
                <div class="mail_list">
                  <div class="right">
                    <h3>Skipped</h3>
                    <p>{{$metadatas['skipped']}} ({{$metadatas['skippedp']}}%)</p>
                  </div>
                </div>
                <div class="mail_list">
                  <div class="right">
                    <h3>Incomplete</h3>
                    <p>{{$metadatas['incomplete']}} ({{$metadatas['incompletep']}}%)</p>
                  </div>
                </div>                      
                <div class="mail_list">
                  <div class="right">
                    <h3>Total</h3>
                    <p>{{$metadatas['total']}}</p>
                  </div>
                </div>                                                                       
              </div>

              <div class="modal fade modalSave" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Select next action</h4>
                        </div>

                        <div class="modal-body">

                          <div class="row">
                            <div class="col-md-6">
                                <input type="radio" name="next_action"  value="pullnext" class="flat" checked="" /> Pull next request
                                <br />
                                <input type="radio" name="next_action"  value="lunch" class="flat" /> Lunch
                                <br />
                                <input type="radio" name="next_action"  value="break" class="flat" /> Break
                                <br />
                            </div>
                            <div class="col-md-6">
                                <input type="radio" name="next_action"  value="meeting" class="flat" /> Meeting
                                <br />
                                <input type="radio" name="next_action"  value="other" class="flat" /> Other
                                <br />
                                <input type="radio" name="next_action"  value="logout" class="flat" /> Log Out
                                <br />
                            </div>                            
                          </div> 

                          <div class="ln"></div>
                          <div class="row">
                            <div class="col-md-12">
                                <input type="radio" class="flat" name="issave" value="pause" checked="" /> Pause
                                <br>                              
                                <input type="radio" class="flat" name="issave" value="skipped" /> Skip
                                <br>
                                <input type="radio" class="flat" name="issave" value="incomplete" /> Incomplete
                                <br>                                
                                <input type="radio" class="flat" name="issave" value="done" /> Save                        
                            </div>                          
                          </div>  

                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                          <button type="submit" class="btn btn-primary">Proceed</button>
                        </div>

                      </div>
                  </form>

                </div>
              </div>              
          </form>
          @else

          <div class="row">
            <div class="col-md-12">

              <h1>No Data Available <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target=".modalNoData">Change Status</button> </h1>
              
              <div class="modal fade modalNoData" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                    <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.status.change')}}" novalidate>
                    {{ csrf_field() }}    
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Select next action</h4>
                      </div>

                      <div class="modal-body">

                        <div class="row">
                          <div class="col-md-6">
                              <input type="radio" name="status"  value="lunch" class="flat" /> Lunch
                              <br />
                              <input type="radio" name="status"  value="break" class="flat" /> Break
                              <br />
                          </div>
                          <div class="col-md-6">
                              <input type="radio" name="status"  value="meeting" class="flat" /> Meeting
                              <br />
                              <input type="radio" name="status"  value="other" class="flat" /> Other
                              <br />
                              <input type="radio" name="status"  value="logout" class="flat" /> Log Out
                              <br />
                          </div>                            
                        </div> 
                      </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Proceed</button>
                      </div>

                    </div>

                </form>
              </div>
              </div>  


            </div>
          </div>

          @endif

        </div>
        <br />

      </div> 
      @else
       
      <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-8 col-md-offset-2" style="margin-top:30px">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Currently Time Out</h3>
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target=".modalLogin">TimeIn</button>
                </div>
              </div>
              <div class="col-md-12"> 
                show latest time recordsss
              </div>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>

      @endif
      <!-- /page content -->

  <div class="modal fade modalLogin" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.status.timein')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Choose campaign</h4>

                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaigns</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="form-control" name="{{$name}}">
                          <option value = ''>Choose option</option>
                          @foreach($campaigns as $v)
                             <option value="{{$v->id}}" >{{$v->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>

  <div class="modal fade modalLogout" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.status.timeout')}}" novalidate>
        {{ csrf_field() }}
           <input type="hidden" name="account_id" value="1">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Are you sure you want to time out?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Proceed</button>
            </div>
          </div>
      </form>

    </div>
  </div>


@endsection 


@section('header-scripts')
<style type="text/css">
	.mb-time{
		font-size: 25px;
	}
  #current-session{
    text-transform: capitalize;
  }
  .row < .clicked{
    background-color: #ebebeb;
  }
  .row < .div-btns{
    padding-top: 5px;
  }
  .ln{
    border-top: 1px solid #e5e5e5;
    margin: 5px 0;
    height: 1px;
  }
  .mb-tc-div span{
    padding-right: 7px;
    font-size: 11px;
    font-weight: bold;
    text-transform: uppercase;    
  }
  .mb-tc-div p{
    margin-bottom: 2px;
  }  
</style>
@endsection

@section('footer-scripts')
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/timer.jquery.min.js"></script> 
<script language="javascript" type="text/javascript" src="{{$asset}}gentella/js/clock.js"></script>
  <script type="text/javascript">

    !function(a) {
        a.mb_lib = {
            mb_ajax: function(b) {
                var c = {
                    data: "null",
                    url: "null",
                    type: "post",
                    attachto: "null"
                };
                if (b) a.extend(c, b);
                a.ajax({
                    url: c["url"],
                    data: c["data"],
                    type: c["type"],
                    success: function(b) {
                      console.log(b);
                    },
                    error: function(a) {

                    }
                });
            }
        };      
    }(jQuery); 

    var mb_session = 'logged in';
    @if($sec_passed)
      var mb_session = "{{$session['status']}}";
    @endif
    $('#current-session').text(mb_session);

    var base = "{{URL::to('/')}}";
    @foreach($status as $k => $v)
      console.log("{{$sec_passed['status'].' '.$k}}");
      @if($sec_passed && $sec_passed['status'] == $v)
        $('#{{$k}}').timer({seconds: {{$sec_passed['lapsed']}},format: '%H:%M:%S'});
        $('.{{$k}}-resume').hide();
        $('.{{$k}}-pause').show();
      @else
        $('#{{$k}}').timer({seconds: {{$gn->ttosec($session[$k])}},format: '%H:%M:%S'});
        $('#{{$k}}').timer('pause'); 
      @endif   
    @endforeach

    @foreach($status as $k => $v)
    
      $('.{{$k}}-resume').on('click',function(){
        @foreach($status as $k2 => $v2)
          @if($k2 != $k) 
            if($('#{{$k2}}').data('seconds')){
              $('#{{$k2}}').timer('pause');
              $('.{{$k2}}-pause').hide();
              $('.{{$k2}}-resume').show();
            }         
          @endif
        @endforeach       
        $('#{{$k}}').timer('resume');
        var $this = $(this);
         console.log($this);
        $this.hide();
        $this.siblings().show();    
      });
    @endforeach    

    $('.resume').on('click',function(){
      var $this = $(this);
      var mb_session = $this.data('mtype');
      $('#current-session').text(mb_session);
    });

    $('.pause').on('click',function(e){
      if(e.which){
        $('.modalBacktowork').modal('show');     
      }
    });

    $(document).ready(function(){
       $('#clock1').jsclock("{{$time}}");
    });   

    $('.div-btns').on('click',function(e){
      var $this = $(this);
      $('.div-btns').removeClass('clicked');
      $('.div-btns').addClass('inactive');
      $this.addClass('clicked');
      $this.removeClass('inactive');

      var data = {_token:"{{csrf_token()}}",user:"{{Auth::user()->id}}",status:$('#current-session').text()};

      @foreach($status as $k => $v)
          data["{{$k}}"] = "{{$session[$k]}}";
          if($('#{{$k}}').data('seconds')){
            data["{{$k}}"] = $('#{{$k}}').data('seconds');
          }else{
            data["{{$k}}"] = 0;
          }         
      @endforeach 

      if(e.which){
        var a = {
            data: data,
            url: base + "/status/update"
        };
        $.mb_lib.mb_ajax(a);

      }

    });

  </script>
@endsection