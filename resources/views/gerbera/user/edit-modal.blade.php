


<button type="button" class="btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".modalEditUser{{$id}}"></button>


<div class="modal fade modalEditUser{{$id}} " tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">


      <div class="x_panel">
        <div class="x_title">
          <h2>Edit User</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form class="form-horizontal form-label-left" method="post" action="{{route('gerbera.user.update',$id)}}" novalidate>
           {{ csrf_field() }}
           <div class="row">
            <div class="col-md-10 col-md-offset-1">
              {!!view('gerbera.forms.text',['label' => 'Name','name' => 'name','value' => $user->name])!!}
              {!!view('gerbera.forms.text',['label' => 'Email','name' => 'email','value' => $user->email])!!}

              <div class="item form-group">
                <label class="control-label col-md-3" for="name">Password
                </label>
                <div class="col-md-9">
                  <input class="form-control col-md-7 col-xs-12" name="password" type="text" value="{{$user->settings['password']}}" disabled>
                </div>
              </div>

              {!!view('gerbera.forms.text',['label' => 'Campaign','name' => 'campaign_id','value' => $gn->userOptions_getter($user->options,'campaign_id')])!!}
              {!!view('gerbera.forms.dropdown',['label' => 'User type','name' => 'user_type','value' => $user->user_type,'choices' => ['admin' => 'admin','user' => 'user','agent' => 'agent']])!!}

            </div>                     
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <button  type="submit" class="btn btn-success pull-right">Update</button>
            </div>
          </div>
        </form>
      </div>

    </div>

  </div>
</div>
</div>

