<?php $asset = URL::asset('/'); ?> 
@extends('gerbera.master2')

@section('title', 'dashboard')

@section('content')


      <div class="right_col" role="main">
        <div class="row">
          <div class="col-md-12" style="margin-top:30px">
            <div class="dashboard_graph">

              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Users</h3>
                </div>
                <div class="col-md-6">
                 {!!view('gerbera.user.add-modal',['route' => 'gerbera.user.create'])!!}
                </div>
              </div>
              <div class="col-md-12"> 

		              <div class="x_panel">
		                <div class="x_content">
			                  <table class="table table-striped responsive-utilities jambo_table bulk_action">
			                    <thead>
			                      <tr class="headings">
			                        <th class="column-title">Name</th>
			                        <th class="column-title">Email</th>
			                        <th class="column-title">User Type</th>
			                        <th class="column-title">#</th>
			                      </tr>
			                    </thead>
			                    <tbody>
			                      @foreach($users as $user)
			                      <tr class="even pointer">
			                        <td>{{$user->name}}</td>
			                        <td>{{$user->email}}</td>
			                        <td>{{($user->settings?$user->settings->user_type:'')}}</td>
			                        <td>
			                        	{!!view('gerbera.modals.delete',['label' =>'user '.$user->name,'id' => $user->id,'route' => 'gerbera.user.destroy'])!!}
			                        	{!!view('gerbera.user.edit-modal',['user' => $user,'id' => $user->id,'gn' => $gn])!!}
			                        </td>
			                      </tr>
			                      @endforeach
			                    </tbody>
			                  </table>
		                </div>
		              </div>   


              </div>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>

@endsection 


@section('header-scripts')
@endsection

@section('footer-scripts')

@endsection